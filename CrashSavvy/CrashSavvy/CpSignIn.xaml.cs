﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrashSavvy
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CpSignIn : ContentPage
	{
		public CpSignIn ()
		{
			InitializeComponent ();
		}

        private async void BtnSignInPage_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new CpCreateIncidentReport());
        }
    }
}