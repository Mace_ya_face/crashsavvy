﻿using Acr.UserDialogs;
using CrashSavvy.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrashSavvy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CpEditCar : ContentPage
    {
        string localRegNo;
        protected override void OnAppearing()
        {
            base.OnAppearing();
            var car = App.Database.GetCar(localRegNo);

            entFirstName.Text = car.Result.FirstName;
            entLastName.Text = car.Result.LastName;
            entYearOfBirth.MaximumDate = DateTime.Now;
            entMobile.Text = car.Result.Mobile;
            entLandline.Text = car.Result.Landline;
            entAddressLine1.Text = car.Result.AddressLine1;
            entAddressLine2.Text = car.Result.AddressLine2;
            entPostcode.Text = car.Result.PostCode;
            entEmailAddress.Text = car.Result.EmailAddress;
            entInsurer.Text = car.Result.Insurer;
            entInsuranceNumber.Text = car.Result.InsuranceNo;
            entRegNo.Text = car.Result.RegNo;
            entCarMake.Text = car.Result.CarMake;
            entCarModel.Text = car.Result.CarModel;
            for (int i = 1900; i < DateTime.Now.Year + 1; ++i)
            {
                entCarYear.Items.Add(i.ToString());
            }
            entCarYear.SelectedIndex = entCarYear.Items.Count;
        }

        public CpEditCar(string regNo)
        {
            InitializeComponent();
            localRegNo = regNo;
        }

        private async void SaveEditButton_Clicked(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Saving", MaskType.Black);

            var entryNull = false;

            if (entFirstName.Text == "")
            {
                entFirstName.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entFirstName.BackgroundColor = Color.Transparent;
            }

            if (entLastName.Text == "")
            {
                entLastName.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entLastName.BackgroundColor = Color.Transparent;
            }

            if (entAddressLine1.Text == "")
            {
                entAddressLine1.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entEmailAddress.BackgroundColor = Color.Transparent;
            }

            if (entInsurer.Text == "")
            {
                entInsurer.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entInsuranceNumber.BackgroundColor = Color.Transparent;
            }

            if (entInsuranceNumber.Text == "")
            {
                entInsuranceNumber.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entInsuranceNumber.BackgroundColor = Color.Transparent;
            }

            if (entCarMake.Text == "")
            {
                entCarMake.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entCarMake.BackgroundColor = Color.Transparent;
            }

            if (entCarModel.Text == "")
            {
                entCarModel.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entCarModel.BackgroundColor = Color.Transparent;
            }

            if (entryNull == true) { UserDialogs.Instance.HideLoading(); return; }

            entFirstName.BackgroundColor = Color.Transparent;
            entLastName.BackgroundColor = Color.Transparent;
            entEmailAddress.BackgroundColor = Color.Transparent;
            entAddressLine1.BackgroundColor = Color.Transparent;
            entAddressLine2.BackgroundColor = Color.Transparent;
            entPostcode.BackgroundColor = Color.Transparent;
            entInsurer.BackgroundColor = Color.Transparent;
            entInsuranceNumber.BackgroundColor = Color.Transparent;
            entCarMake.BackgroundColor = Color.Transparent;
            entCarModel.BackgroundColor = Color.Transparent;

            try
            {
                var newCar = new Car
                {
                    FirstName = entFirstName.Text.ToString(),
                    LastName = entLastName.Text.ToString(),
                    YearOfBirth = entYearOfBirth.Date,
                    EmailAddress = entEmailAddress.Text.ToString(),
                    AddressLine1 = entAddressLine1.Text.ToString(),
                    AddressLine2 = entAddressLine2.Text.ToString(),
                    PostCode = entPostcode.Text.ToString(),
                    Insurer = entInsurer.Text.ToString(),
                    InsuranceNo = entInsuranceNumber.Text.ToString(),
                    RegNo = entRegNo.Text.ToString(),
                    CarMake = entCarMake.Text.ToString(),
                    CarModel = entCarModel.Text.ToString(),
                    CarYear = entCarYear.SelectedItem.ToString()
                };

                await Task.Run(() => App.Database.UpdateCar(newCar));

                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Alert("We couldn't save your information. Please try again. Error: " + ex, "Sorry...");
                UserDialogs.Instance.HideLoading();
            }

            var toastConfig = new ToastConfig("Car Saved!")
            {
                Position = ToastPosition.Bottom,
                Duration = TimeSpan.FromSeconds(3)
            };
            UserDialogs.Instance.Toast(toastConfig);

            await Navigation.PopAsync();
        }

        private async void CancelButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}