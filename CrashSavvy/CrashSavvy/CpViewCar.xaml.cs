﻿using Acr.UserDialogs;
using CrashSavvy.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrashSavvy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CpViewCar : ContentPage
    {
        string localRegNo;

        protected override void OnAppearing()
        {
            base.OnAppearing();
            PopulateCarDetails(localRegNo);
        }

        public CpViewCar(string regNo)
        {
            InitializeComponent();
            localRegNo = regNo;
        }

        private async void EditButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CpEditCar(lblRegNo.Text));
        }

        private async void ViewIncidentsButton_Clicked(object sender, EventArgs e)
        {

        }

        private async void DeleteButton_Clicked(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Deleting", MaskType.Black);

            try
            {
                var car = new Car()
                {
                    RegNo = lblRegNo.Text
                };
                await Task.Run(() => App.Database.DeleteCarAsync(car));

                UserDialogs.Instance.HideLoading();

                var toastConfig = new ToastConfig("Car Deleted!")
                {
                    Position = ToastPosition.Bottom,
                    Duration = TimeSpan.FromSeconds(3)
                };
                UserDialogs.Instance.Toast(toastConfig);

                await Navigation.PopAsync();
            }
            catch
            {
                UserDialogs.Instance.HideLoading();
                UserDialogs.Instance.Alert("There was an issue deleteing this car. Please try again.", "Sorry...");
            }
        }

        private void PopulateCarDetails(string regNo)
        {
            var car = App.Database.GetCar(regNo);
            lblName.Text = car.Result.FirstName + " " + car.Result.LastName;
            lblRegNo.Text = car.Result.RegNo;
            lblCarMake.Text = car.Result.CarMake;
            lblCarModel.Text = car.Result.CarModel;
            lblCarYear.Text = car.Result.CarYear;
        }
    }
}