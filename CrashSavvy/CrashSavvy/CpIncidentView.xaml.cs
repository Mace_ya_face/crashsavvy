﻿using Acr.UserDialogs;
using CrashSavvy.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrashSavvy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CpIncidentView : ContentPage
    {
        int incidentId;
        protected override void OnAppearing()
        {
            base.OnAppearing();
            PopulateIncidentDetails();
        }

        public CpIncidentView()
        {
            InitializeComponent();
        }

        private async void EditButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CpEditIncident());
        }

        private async void DeleteButton_Clicked(object sender, EventArgs e)
        {
            UserDialogs.Instance.ShowLoading("Deleting", MaskType.Black);

            try
            {
                var incident = new Incident()
                {
                    Id = incidentId
                };
                await Task.Run(() => App.Database.DeleteIncidentAsync(incident));

                UserDialogs.Instance.HideLoading();

                var toastConfig = new ToastConfig("Incident Deleted!")
                {
                    Position = ToastPosition.Bottom,
                    Duration = TimeSpan.FromSeconds(3)
                };
                UserDialogs.Instance.Toast(toastConfig);

                await Navigation.PopAsync();
            }
            catch
            {
                UserDialogs.Instance.HideLoading();
                UserDialogs.Instance.Alert("There was an issue deleteing this incident. Please try again.", "Sorry...");
            }
        }

        private async void PopulateIncidentDetails()
        {
            try
            {
                var incident = await App.Database.GetIncident(incidentId);
                var car = await App.Database.GetCar(incident.RegNo);

                lblRegNo.Text = incident.RegNo;
                lblCarMake.Text = car.CarMake;
                lblCarModel.Text = car.CarModel;
                lblCarYear.Text = car.CarYear;
                lblIncidentDescription.Text = incident.IncidentDescription;
                lblSecondPartyName.Text = incident.SecondPartyFirstName + " " + incident.SecondPartyLastName;
                lblSecondPartyAddressLine1.Text = incident.SecondPartyAddressLine1;
                lblSecondPartyAddressLine2.Text = incident.SecondPartyAddressLine2;
                lblSecondPartyPostcode.Text = incident.SecondPartyPostcode;
                lblSecondPartyMobile.Text = incident.SecondPartyMobile;
                lblSecondPartyLandline.Text = incident.SecondPartyLandline;
                lblSecondPartyEmail.Text = incident.SecondPartyEmail;
                lblSecondPartyInsurer.Text = incident.SecondPartyInsurer;
                lblSecondPartyPolicyNo.Text = incident.SecondPartyPolicyNo;
                lblSecondPartyVehMake.Text = incident.SecondPartyVehMake;
                lblSecondPartyVehModel.Text = incident.SecondPartyVehModel;
                lblSecondPartyVehYear.Text = incident.SecondPartyVehYear;
                lblSecondPartyVehImgDirectory.Text = incident.SecondPartyVehImgDirectory;
                lblSecondPartyVehRegNo.Text = incident.SecondPartyVehRegNo;
                lblIncidentTime.Text = incident.IncidentTime.ToString();
                lblIncidentCoords.Text = incident.IncidentCoordLat.ToString() + " | " + incident.IncidentCoordLon.ToString();
                lblIncidentAddress.Text = incident.IncidentAddress;
                lblDirectionOfTravel.Text = incident.DirectionOfTravel;
                lblSecondPartyDirectionOfTravel.Text = incident.SecondPartyDirectionOfTravel;
                lblIncidentSceneImagesDirectory.Text = incident.IncidentSceneImagesDirectory;
                lblEnviromentalConditions.Text = incident.EnviromentalConditions;
                lblWitnessNotes.Text = incident.WitnessNotes;
                lblPoliceNotes.Text = incident.PoliceNotes;
                lblCarImgDirectory.Text = incident.CarImgDirectory;
            }
            catch
            {
                UserDialogs.Instance.Alert("There was an issue getting the incident data. Please try again.", "Sorry...");
                await Navigation.PopAsync();
            }
        }
    }
}