﻿using Acr.UserDialogs;
using CrashSavvy.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrashSavvy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CpEditIncident : ContentPage
    {
        int incidentId;
        string incidentRegNo;

        public CpEditIncident()
        {
            InitializeComponent();
        }

        private async void SaveEditButton_Clicked()
        {
            UserDialogs.Instance.ShowLoading("Saving", MaskType.Black);

            var entryNull = false;

            if (entFirstName.Text == "")
            {
                entFirstName.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entFirstName.BackgroundColor = Color.Transparent;
            }

            if (entLastName.Text == "")
            {
                entLastName.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entLastName.BackgroundColor = Color.Transparent;
            }

            if (entAddressLine1.Text == "")
            {
                entAddressLine1.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entEmailAddress.BackgroundColor = Color.Transparent;
            }

            if (entInsurer.Text == "")
            {
                entInsurer.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entInsuranceNumber.BackgroundColor = Color.Transparent;
            }

            if (entInsuranceNumber.Text == "")
            {
                entInsuranceNumber.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entInsuranceNumber.BackgroundColor = Color.Transparent;
            }

            if (entCarMake.Text == "")
            {
                entCarMake.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entCarMake.BackgroundColor = Color.Transparent;
            }

            if (entCarModel.Text == "")
            {
                entCarModel.BackgroundColor = Color.PaleVioletRed;
                entryNull = true;
            }
            else
            {
                entCarModel.BackgroundColor = Color.Transparent;
            }

            if (entryNull == true) { UserDialogs.Instance.HideLoading(); return; }

            entFirstName.BackgroundColor = Color.Transparent;
            entLastName.BackgroundColor = Color.Transparent;
            entEmailAddress.BackgroundColor = Color.Transparent;
            entAddressLine1.BackgroundColor = Color.Transparent;
            entAddressLine2.BackgroundColor = Color.Transparent;
            entPostcode.BackgroundColor = Color.Transparent;
            entInsurer.BackgroundColor = Color.Transparent;
            entInsuranceNumber.BackgroundColor = Color.Transparent;
            entCarMake.BackgroundColor = Color.Transparent;
            entCarModel.BackgroundColor = Color.Transparent;

            try
            {
                var newIncident = new Incident
                {
                    Id = incidentId,

                    RegNo = incidentRegNo,

                    IncidentDescription = entIncidentDescription.Text,

                    SecondPartyFirstName = entSecondPartyFirstName.Text,

                    SecondPartyLastName= entSecondPartyLastName.Text,

                    SecondPartyAddressLine1= entSecondPartyAddressLine1.Text,

                    SecondPartyAddressLine2 = entSecondPartyAddressLine2.Text,

                    SecondPartyPostcode = entSecondPartyPostcode.Text,

                    SecondPartyMobile = entSecondPartyMobile.Text,

                    SecondPartyLandline = entSecondPartyLandline.Text,

                    SecondPartyEmail = entSecondPartyEmail.Text,

                    SecondPartyInsurer = entSecondPartyInsurer.Text,

                    SecondPartyPolicyNo = entSecondPartyPolicyNo.Text,

                    SecondPartyVehMake = entSecondPartyVehMake.Text,

                    SecondPartyVehModel = entSecondPartyVehModel.Text,

                    SecondPartyVehYear= entSecondPartyVehYear.Text,

                    SecondPartyVehImgDirectory = entSecondPartyVehImgDirectory.Text,

                    SecondPartyVehRegNo = entSecondPartyVehRegNo.Text,

                    IncidentTime = entIncidentTime,

                    IncidentCoordLat = entIncidentCoordsLat.Text,

                    IncidentCoordLon = entIncidentCoordsLon.Text,

                    DirectionOfTravel = entDirectionOfTravel.Text,

                    SecondPartyDirectionOfTravel = entSecondPartyDirectionOfTravel.Text,

                    IncidentSceneImagesDirectory = entIncidentSceneImagesDirectory.Text,

                    EnviromentalConditions = entEnviromentalConditions.Text,

                    WitnessNotes = entWitnessNotes.Text,

                    PoliceNotes = entPoliceNotes.Text,

                    CarImgDirectory = entCarImgDirectory.Text
                };

                await Task.Run(() => App.Database.UpdateIncident(newIncident));

                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.Alert("We couldn't save your information. Please try again.", "Sorry...");
                UserDialogs.Instance.HideLoading();
            }

            var toastConfig = new ToastConfig("Car Saved!")
            {
                Position = ToastPosition.Bottom,
                Duration = TimeSpan.FromSeconds(3)
            };
            UserDialogs.Instance.Toast(toastConfig);

            await Navigation.PopAsync();
        }
    }
}