﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using CrashSavvy.Logic;
using iText.IO.Image;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;

namespace CrashSavvy.Droid
{
    class CrashIO
    {
        public static async void writeDatabaseIncidentDetails(Incident incidentData)
        {
            try
            {
                await App.Database.SaveIncident(incidentData);
            }
            catch
            {
                UserDialogs.Instance.Alert("There was an issue trying to save your incident data. Please try again.", "Sorry...");
            }
        }

        public static async void writePDF(Incident incidentData)
        {
            try
            {
                Car car = await App.Database.GetCar(incidentData.RegNo);
                var writer = new PdfWriter(
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments))
                    + car.RegNo + "_" + incidentData.IncidentTime.Day + incidentData.IncidentTime.Month + incidentData.IncidentTime.Year);
                var pdfDoc = new PdfDocument(writer);
                var document = new Document(pdfDoc);
                float[] pointColumnWidths = { 150f, 150f };
                var table = new Table(pointColumnWidths);
                table.AddCell("Driver First Name");
                table.AddCell(incidentData.SecondPartyFirstName);
                table.AddCell("Driver Last Name");
                table.AddCell(incidentData.SecondPartyLastName);
                table.AddCell("Driver Mobile No.");
                table.AddCell(incidentData.SecondPartyMobile);
                table.AddCell("Driver Mobile No.");
                table.AddCell(incidentData.SecondPartyLandline);
                table.AddCell("Driver Email");
                table.AddCell(incidentData.SecondPartyEmail);
                table.AddCell("Driver Address Line 1");
                table.AddCell(incidentData.SecondPartyAddressLine1);
                table.AddCell("Driver Address Line 2");
                table.AddCell(incidentData.SecondPartyAddressLine2);
                table.AddCell("Driver Insurer");
                table.AddCell(incidentData.SecondPartyInsurer);
                table.AddCell("Driver Policy No.");
                table.AddCell(incidentData.SecondPartyPolicyNo);
                table.AddCell("Driver Vehichle Make");
                table.AddCell(incidentData.SecondPartyVehMake);
                table.AddCell("Driver Vehicle Model");
                table.AddCell(incidentData.SecondPartyVehModel);
                table.AddCell("Driver Vehicle Year");
                table.AddCell(incidentData.SecondPartyVehYear.ToString());
                table.AddCell("Driver Licence Plate");
                table.AddCell(incidentData.SecondPartyVehRegNo);
                table.AddCell("Drive Vehicle Image");
                var data = ImageDataFactory.Create(incidentData.SecondPartyVehImgDirectory);
                var img = new Image(data);
                table.AddCell(img.SetAutoScale(true));
                table.AddCell("Your First Name");
                table.AddCell(car.FirstName);
                table.AddCell("Your Last Name");
                table.AddCell(car.LastName);
                table.AddCell("Your Mobile No.");
                table.AddCell(car.Mobile);
                table.AddCell("Your Landline No.");
                table.AddCell(car.Landline);
                table.AddCell("Your Email");
                table.AddCell(car.EmailAddress);
                table.AddCell("Your Address Line 1");
                table.AddCell(car.AddressLine1);
                table.AddCell("Your Address Line 2");
                table.AddCell(car.AddressLine2);
                table.AddCell("Your Postcode");
                table.AddCell(car.PostCode);
                table.AddCell("Your Insurer");
                table.AddCell(car.Insurer);
                table.AddCell("Your Policy Number");
                table.AddCell(car.InsuranceNo);
                table.AddCell("Your Vehicle Make");
                table.AddCell(car.CarMake);
                table.AddCell("Your Vehicle Model");
                table.AddCell(car.CarModel);
                table.AddCell("Your Vehicle Year");
                table.AddCell(car.CarYear.ToString());
                table.AddCell("Your Licence Plate No.");
                table.AddCell(car.CarYear.ToString());
                table.AddCell("Your Vehicle Year");
                data = ImageDataFactory.Create(incidentData.CarImgDirectory);
                img = new Image(data);
                table.AddCell(img.SetAutoScale(true));
                table.AddCell("Time Of Incident");
                table.AddCell(incidentData.IncidentTime.ToString());
                table.AddCell("Address Of Incident");
                table.AddCell(incidentData.IncidentAddress);
                table.AddCell("Coordinates Of Incident");
                table.AddCell(incidentData.IncidentCoordLon.ToString() + " | " + incidentData.IncidentCoordLat.ToString());
                table.AddCell("Your Travel Direction");
                table.AddCell(incidentData.DirectionOfTravel);
                table.AddCell("Driver Travel Direction");
                table.AddCell(incidentData.SecondPartyDirectionOfTravel);
                table.AddCell("Event Description");
                table.AddCell(incidentData.IncidentDescription);
                table.AddCell("Enviromental Conditions");
                table.AddCell(incidentData.EnviromentalConditions);
                table.AddCell("General Witness Details");
                table.AddCell(incidentData.WitnessNotes);
                table.AddCell("Police Witness Details");
                table.AddCell(incidentData.PoliceNotes);

                document.Add(table);

                document.Close();
            }
            catch
            {
                UserDialogs.Instance.Alert("There was an issue trying to save your PDF file. Please try again.", "Sorry...");
            }
        }
    }
}