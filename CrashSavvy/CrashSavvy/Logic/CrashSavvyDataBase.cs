﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CrashSavvy.Logic
{
    public class CrashSavvyDB
    {
        readonly SQLiteAsyncConnection _database;

        public CrashSavvyDB(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Car>().Wait();
            _database.CreateTableAsync<Incident>().Wait();
        }

        public Task<List<Car>> GetCars()
        {
            return _database.Table<Car>().ToListAsync();
        }

        public Task<Car> GetCar(string regNo)
        {
            return _database.Table<Car>().Where(t => t.RegNo == regNo).FirstAsync();
        }

        public Task<int> SaveCar(Car car)
        {
            return _database.InsertAsync(car);
        }

        public Task<int> UpdateCar(Car car)
        {
            return _database.UpdateAsync(car);
        }

        public Task<int> DeleteCarAsync(Car car)
        {
            return _database.DeleteAsync(car);
        }

        public Task<List<Incident>> GetIncidents()
        {
            return _database.Table<Incident>().ToListAsync();
        }

        public Task<Incident> GetIncident(int Id)
        {
            return _database.Table<Incident>().Where(t => t.Id == Id).FirstAsync();
        }

        public Task<int> SaveIncident(Incident incident)
        {
            return _database.UpdateAsync(incident);
        }

        public Task<int> UpdateIncident(Incident incident)
        {
            return _database.InsertAsync(incident);
        }

        public Task<int> DeleteIncidentAsync(Incident incident)
        {
            return _database.DeleteAsync(incident);
        }
    }

    [Table("Car")]
    public class Car
    {
        [PrimaryKey]
        public string RegNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime YearOfBirth { get; set; }
        public string Mobile { get; set; }
        public string Landline { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string PostCode { get; set; }
        public string EmailAddress { get; set; }
        public string Insurer { get; set; }
        public string InsuranceNo { get; set; }
        public string CarMake { get; set; }
        public string CarModel { get; set; }
        public string CarYear { get; set; }
    }

    [Table("Incident")]
    public class Incident
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [ForeignKey(typeof(Car))]
        public string RegNo { get; set; }
        public string IncidentDescription { get; set; }
        public string SecondPartyFirstName { get; set; }
        public string SecondPartyLastName { get; set; }
        public string SecondPartyAddressLine1 { get; set; }
        public string SecondPartyAddressLine2 { get; set; }
        public string SecondPartyPostcode { get; set; }
        public string SecondPartyMobile { get; set; }
        public string SecondPartyLandline { get; set; }
        public string SecondPartyEmail { get; set; }
        public string SecondPartyInsurer { get; set; }
        public string SecondPartyPolicyNo { get; set; }
        public string SecondPartyVehMake { get; set; }
        public string SecondPartyVehModel { get; set; }
        public string SecondPartyVehYear { get; set; }
        public string SecondPartyVehImgDirectory { get; set; }
        public string SecondPartyVehRegNo { get; set; }
        public DateTime IncidentTime { get; set; }
        public int IncidentCoordLat { get; set; }
        public int IncidentCoordLon { get; set; }
        public string IncidentAddress { get; set; }
        public string DirectionOfTravel { get; set; }
        public string SecondPartyDirectionOfTravel { get; set; }
        public string IncidentSceneImagesDirectory { get; set; }
        public string EnviromentalConditions { get; set; }
        public string WitnessNotes { get; set; }
        public string PoliceNotes { get; set; }
        public string CarImgDirectory { get; set; }
    }
}
