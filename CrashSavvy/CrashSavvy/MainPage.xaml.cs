﻿using CrashSavvy.Droid;
using CrashSavvy.Logic;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CrashSavvy
{
    public partial class MainPage : ContentPage
    {
        protected override void OnAppearing()
        {
            base.OnAppearing();
            PopulateCarList();
        }

        public MainPage()
        {
            NavigationPage MainPage = new NavigationPage();

            InitializeComponent();
        }

        private async void RegisterButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CpRegister());
            InitializeComponent();
            PopulateCarList();
        }

        private async void SignInButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CpSignIn());
        }

        private async void CarLabel_Clicked(string regNo)
        {
            await Navigation.PushAsync(new CpViewCar(regNo));
            InitializeComponent();
        }

        private async void PopulateCarList()
        {
            var carList = await App.Database.GetCars();
            
            foreach(Car car in carList)
            {
                var carLabel = new Label
                {
                    Text = car.RegNo
                };

                carLabel.GestureRecognizers.Add(new TapGestureRecognizer {
                    Command = new Command(() => CarLabel_Clicked(car.RegNo))
                });

                listCars.Children.Add(carLabel);
            }
        }
    }
}