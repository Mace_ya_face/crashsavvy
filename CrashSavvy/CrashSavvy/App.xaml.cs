﻿using CrashSavvy.Logic;
using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace CrashSavvy
{
    public partial class App : Application
    {
        static CrashSavvyDB database;

        public static CrashSavvyDB Database
        {
            get
            {
                if (database == null)
                {
                    database = new CrashSavvyDB(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CrashSavvyDB.db3"));
                }
                return database;
            }
        }

        public App()
        {
            MainPage = new NavigationPage(new MainPage());
            InitializeComponent();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
