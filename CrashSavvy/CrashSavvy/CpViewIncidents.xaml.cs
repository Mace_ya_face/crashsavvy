﻿using CrashSavvy.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrashSavvy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CpViewIncidents : ContentPage
    {
        protected override void OnAppearing()
        {
            base.OnAppearing();
            PopulateIncidents();
        }

        public CpViewIncidents()
        {
            InitializeComponent();
        }

        private async void IncidentLabel_Clicked(int incidentId)
        {

        }

        private async void PopulateIncidents()
        {
            var incidentList = await App.Database.GetIncidents();

            foreach (Incident incident in incidentList)
            {
                var incidentLabel = new Label
                {
                    Text = incident.IncidentAddress
                };

                incidentLabel.GestureRecognizers.Add(new TapGestureRecognizer
                {
                    Command = new Command(() => IncidentLabel_Clicked(incident.Id))
                });

                listIncidents.Children.Add(incidentLabel);
            }
        }
    }
}